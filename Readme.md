# Read Me

โปรเจค Spring MVC พื้นฐานจริงๆ ไม่มีอะไรมากมาย แค่รันเว็บง่ายๆ โดยการใช้ maven 
สร้างโปรเจค spring mvc แล้วรันด้วย jetty 

## v3.0

1. สร้าง form และใช้ jstl form: พร้อมกับดัก error
2. สร้าง add() ใน service และ memberAdd() ใน controller 
3. bean validator ด้วย hibernate validator ให้กับ domain 
4. ทำให้แสดงผล form และ redirect ทั้งกรณีผิดพลาด และกรณีถูกต้อง

## v2.0

1. สร้าง Domain class (Model) แต่ยังใช้ Lombok มาช่วยเพิ่ม Getter Setter ไม่ได้ เสียดาย ตอนนี้เลยต้อง generate เองไปก่อน
2. สร้าง Controller โดยให้ request mapping "/member/list" ส่งค่า preset ของ member ออกมา
    * preset data นี้ไปแอบสร้างไว้ใน Service class โดยใน service ก็จะมี findAll() ไว้ให้แล้วด้วย
    * อันนี้ทำแทน database จริงๆ ไปพลางๆ เพื่อความง่ายในการทดสอบ
3. เพิ่ม basedPackage ไว้ใน AppConfig ให้เห็น service ด้วย พร้อมทั้งกำหนด ViewResolver ให้รู้จัก jsp directory และนามสกุลไฟล์ .jsp
4. สร้าง view ชื่อเดียวกับ method ที่ map ไว้ใน /{controller}/{method}/ แล้ว return list ของ domain class ออกไป
5. Thai Baht (฿) ใช้ HTML Entity &3647; แสดงผลได้ปกติ แต่่พิมพ์ตรงๆ ไม่ได้

## URL ปัจจุบัน

1. [http://localhost:8080/hi](http://localhost:8080/hi)
    * แสดงผล hello world
2. [http://localhost:8080/member/list](http://localhost:8080/member/list)
    * แสดงผล list ของ preset data จาก service class


