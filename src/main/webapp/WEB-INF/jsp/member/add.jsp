<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Member</title>
</head>
<body>

<h1>Add a member</h1>
<form:form method="POST" modelAttribute="member">
        First Name:<input type="text" name="firstName" />
            <form:errors path="firstName" cssClass="error"></form:errors><br/>
        Last Name:<input type="text" name="lastName" />
            <form:errors path="lastName" cssClass="error"></form:errors><br/>
        Email:<input type="text" name="email" />
            <form:errors path="email" cssClass="error"></form:errors><br/>
        Member Score:<input type="text" name="memberScore" />
            <form:errors path="memberScore" cssClass="error"></form:errors><br/>
        <input type="submit" />
</form:form>

</body>
</html>