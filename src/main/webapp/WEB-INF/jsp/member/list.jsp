<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Member List</title>
</head>
<body>

<h1>Member List</h1>

<h2><a href="/member/add">Add a Member</a></h2>

<c:forEach items="${memberList}" var="member">
    <p>
        ${member.firstName} ${member.lastName}<<a href="mailto:${member.email}">${member.email}></a>  	&#3647;${member.memberScore}
    </p>
</c:forEach>

</body>
</html>