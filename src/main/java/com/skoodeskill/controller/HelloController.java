package com.skoodeskill.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by wrj on 7/22/2016 AD.
 */
@Controller
public class HelloController {
    @RequestMapping("hi")
    @ResponseBody
    public String hi() {
        return "Hello, World";
    }
}
