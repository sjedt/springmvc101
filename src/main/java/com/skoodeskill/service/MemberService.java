package com.skoodeskill.service;

import com.skoodeskill.domain.Member;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by wrj on 7/22/2016 AD.
 */
@Service
public class MemberService {
    private List<Member> memberList = new LinkedList<Member>();

    MemberService() {
        Member m1 = new Member();
        m1.setFirstName("Worajedt");
        m1.setLastName("Sitthidumrong");
        m1.setEmail("jedt3d@gmail.com");
        m1.setMemberScore(new BigDecimal(15000));
        memberList.add(m1);

        Member m2 = new Member();
        m2.setFirstName("Noppon");
        m2.setLastName("Srisuriyasataya");
        m2.setEmail("noppon.sri@gmail.com");
        m2.setMemberScore(new BigDecimal(12500));
        memberList.add(m2);

        Member m3 = new Member();
        m3.setFirstName("Supasinee");
        m3.setLastName("Leelawattana");
        m3.setEmail("supasinee.lee@gmail.com");
        m3.setMemberScore(new BigDecimal(17000));
        memberList.add(m3);
    }

    public List<Member> findAll() {
        return memberList;
    }

    public void add(Member member) {
        memberList.add(member);
    }
}
